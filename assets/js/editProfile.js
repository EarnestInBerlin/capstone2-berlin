let params = new URLSearchParams(window.location.search);
let userId = params.get("userId");
let fName = document.querySelector("#firstName");
let lName = document.querySelector('#lastName');
let mobileNo = document.querySelector("#mobileNo");
let email = document.querySelector("#email");
let pwd1 = document.querySelector("#password1")
let pwd2 = document.querySelector("#password2")

let password; //to store password data
//console.log(fName, lName, mobileNo, email, pwd1, pwd2)

fetch(`https://radiant-crag-46557.herokuapp.com/api/users/details/${userId}`)
.then(res => res.json())
.then(data => {
    // console.log(data)

    fName.placeholder = data.firstName;
    lName.placeholder = data.lastName;
    mobileNo.placeholder = data.mobileNo;
    email.placeholder = data.email;
    fName.value = data.firstName;
    lName.value = data.lastName;
    mobileNo.value = data.mobileNo;
    email.value = data.email;
    

    document.querySelector('#editProfile').addEventListener("submit", (e) => {
        e.preventDefault()

        let token = localStorage.getItem('token');

        let firstn = fName.value;
        let lastn = lName.value;
        let mobileNum = mobileNo.value;
        let userEmail = email.value;

        if(pwd1.value === '' && pwd2.value === ''){
            password = data.password
        } else if(pwd1.value === pwd2.value){
            password = pwd1.value
        } 

        fetch('https://radiant-crag-46557.herokuapp.com/api/users', {
            method: "PUT",
            headers:{
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                id: userId,
                firstName: firstn,
                lastName: lastn,
                email: userEmail,
                mobileNo: mobileNum,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                alert("You think you're a new person now because you changed");
                window.location.replace('./profile.html');
            } else {
                alert('Something went wrong.')
            }
        })
    })
})
