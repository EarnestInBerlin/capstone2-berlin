let token = localStorage.getItem("token");
let adminUser = localStorage.getItem("isAdmin") === "true"
let addButton = document.querySelector("#adminButton")

if(adminUser === false || adminUser === null){
	addButton.innerHTML = null
} else {
	addButton.innerHTML = `
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-primary">
			Add course</a>
		</div>
	`
}

if(data.allCourses){
	localStorage.getItem('token', data.allCourses)
	fetch('https://radiant-crag-46557.herokuapp.com/api/')}