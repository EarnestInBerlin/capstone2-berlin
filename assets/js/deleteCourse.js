//check boodle 
//step 1
let params = new URLSearchParams(window.location.search)
let jwtToken = localStorage.getItem('token')
//step 2
let courseId = params.get('courseId')


//step 3
fetch(`https://radiant-crag-46557.herokuapp.com/api/courses/${courseId}`, {
	method: 'DELETE',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${jwtToken}`
	},
	body: JSON.stringify({
		courseId: courseId
	})
})//step 4
.then(res => res.json())
.then(data =>{
	console.log(data)
	if (data === true){
		window.location.replace('./courses.html')
	}else{
		alert("Something went wrong and I don't like it.")
	}
})

/*
Correct from Jomar and Sab

let params = new URLSearchParams(window.location.search)
let jwtToken = localStorage.getItem('token')
let courseId = params.get('courseId')

fetch(`http://localhost:8000/api/courses/${courseId}`, {
	method: 'DELETE',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${jwtToken}`
	},
	body: JSON.stringify({
		courseId: courseId
	})
})
.then(res => res.json())
.then(data =>{
	console.log(data)
	if (data === true){
		window.location.replace('./courses.html')
	}else{
		alert("Something went wrong.")
	}
})

*/