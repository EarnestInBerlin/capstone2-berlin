let formSubmit = document.querySelector("#createCourse")
//add an event listener


//Q: What does preventDefault() do?
//A: it prevents the normal behaviour of an event. In this case, the event is submit and it's default behaviour is to refresh the page when submitting the form
//#Id .class

formSubmit.addEventListener("submit", (e) => {
	e.preventDefault()

let courseName = document.querySelector("#courseName").value
let price = document.querySelector("#coursePrice").value
let description = document.querySelector("#courseDescription").value

let token =localStorage.getItem("token")
console.log(token)

	fetch('https://radiant-crag-46557.herokuapp.com/api/courses', {

		method: 'POST',
		headers: {
			'Content-Type' : 'application/json',
			'Authorization' : `Bearer ${token}`,
		},
		body: JSON.stringify({
			name: courseName,
			description: description,
			price: price
		}) 
	})
	.then(res=> res.json())
	.then(data=> {	
		if(data === true) {
			window.location.replace('./courses.html')
		} else {
			alert("Course Creation Failed. Something Went Wrong")
		}

	})
})